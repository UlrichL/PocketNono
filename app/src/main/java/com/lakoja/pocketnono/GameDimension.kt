package com.lakoja.pocketnono

import android.content.res.Resources
import android.view.MotionEvent
import kotlin.math.floor

class GameDimension {
    private var width = 0
    private var height = 0
    var left = 0.0f
    var top = 0.0f
    var xCount = 0
    var yCount = 0
    var single = 0.0f

    fun matches(w: Int, h: Int): Boolean {
        return w == width && h == height
    }

    /**
     * Determine cell width by screen density.
     */
    fun setup(w: Int, h: Int, lineWidth: Float) {
        if (xCount > 0 && yCount > 0) {
            // TODO design this more clearly: this is the case that there are fixed board dimension settings (and this should
            //   not collide with for example changing screen dimensions)

            width = w
            height = h
            left = (w - (xCount * single)) / 2.0f
            top = (h - (yCount * single)) / 2.0f
        } else {
            setup(w, h, 0, 0, lineWidth)
        }
    }

    /**
     * The fixed cell count variant.
     */
    fun setup(w: Int, h: Int, xc: Int, yc: Int,  lineWidth: Float) {
        width = w
        height = h
        single = Resources.getSystem().displayMetrics.densityDpi / 3.5f // third of an inch
        if (xc > 0 && yc > 0) {
            xCount = xc
            yCount = yc
        } else {
            xCount = floor((w - lineWidth) / (single)).toInt()
            yCount = floor((h - lineWidth) / (single)).toInt()
        }
        left = (w - (xCount * single)) / 2.0f
        top = (h - (yCount * single)) / 2.0f
    }

    fun getLocation(motion: MotionEvent): Pair<Int, Int> {
        val xSquare = floor((motion.x - left) / single).toInt()
        val ySquare = floor((motion.y - top) / single).toInt()

        return Pair(xSquare, ySquare)
    }

    fun isGameField(x: Int, y:Int): Boolean {
        return x >= 1 && x < xCount && y >= 1 && y < yCount
    }
}
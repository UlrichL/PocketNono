package com.lakoja.pocketnono

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import java.io.InvalidClassException

val density = Resources.getSystem().displayMetrics.density

// These are extensions:

val Int.dp: Int
    get() = (this / density).toInt()

val Float.px: Float
    get() = (this * density)

// TODO TypedValue.applyDimension(COMPLEX_UNIT_DIP

fun Paint.with(color: Int): Paint {
    this.color = color
    return this
}

fun RectF.with(left:Float, top:Float, right:Float, bottom: Float): RectF {
    this.left = left
    this.top = top
    this.right = right
    this.bottom = bottom

    return this
}


class Board(context: Context, attrs: AttributeSet?) : View(context, attrs), View.OnTouchListener {
    companion object {
        var instance: Board? = null // TODO better solution here?
    }

    private val fillPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = Color.CYAN
    }

    private val linePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = Color.BLUE
        strokeWidth = 3f.px
    }

    private val thinPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = Color.BLACK
        strokeWidth = 1f.px
    }

    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = Color.BLACK
        textSize = 12f.px
        textAlign = Paint.Align.LEFT
    }

    private val dimension = GameDimension()
    private var game = State()
    private var solution = Solution()

    private var hasFixedSettings = false
    private val area = RectF()
    private var hasValidSolution = false
    private var lastX = -1
    private var lastY = -1

    init {
        this.setOnTouchListener(this)
        instance = this

        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.Board,
            0, 0).apply {

            try {
                val cw = getInteger(R.styleable.Board_cellWidth, 0)
                val ch = getInteger(R.styleable.Board_cellHeight, 0)
                var vals = getString(R.styleable.Board_cellValues)

                if (cw > 0 && ch > 0 && vals != null && vals.isNotEmpty()) {
                    vals = vals.trim();
                    dimension.setup(width, height, cw, ch, linePaint.strokeWidth) // NOTE/TODO width and height are still 0 here

                    val stateValues = mutableListOf<StateValue>()
                    for (v in vals.split(",")) {
                        stateValues.add(StateValue.from(v.toByte()))
                    }

                    // A number of field values plus commas inbetween?
                    val gw = cw-1;
                    val gh = ch-1;
                    if (stateValues.size == gw*gh) {
                        game.setup(gw, gh, stateValues.toTypedArray())
                        solution.setup(gw, gh, stateValues.toTypedArray())

                        hasFixedSettings = true
                    }
                }
            } finally {
                recycle()
            }
        }

    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        // Draw a number of squares some parts of an inch wide
        // Consider the line width as part of the squares; so there is only a single line width additional inset;
        //   which is only considered during count calculation (and thus conversely for the insets)


        // TODO there must be some "onLayoutFinished"? maybe Lifecycle.Event.ON_CREATE?
        //   also consider restore and save and fixed settings

        if (!dimension.matches(width, height)) {
            dimension.setup(width, height, linePaint.strokeWidth)
            textPaint.textSize = dimension.single / 2 - 8
        }

        // TODO still lots of -1 (and now +1)

        val xGame = dimension.xCount-1
        val yGame = dimension.yCount-1
        if (!game.matches(xGame, yGame)) {
            // TODO combine with restore

            game.setup(xGame, yGame)
        }
        if (!solution.matches(xGame, yGame)) {
            solution.setup(xGame, yGame, 0.5f)
        }

        canvas.apply {
            drawRect(0f, 0f, width.toFloat(), height.toFloat(), fillPaint.with(Color.WHITE))

            val padding = linePaint.strokeWidth*2
            val radius = (dimension.single - padding*2)/2

            // TODO proper marking
            val paint = if (hasValidSolution) fillPaint.with(0xff7dff7d.toInt()) else fillPaint.with(Color.GRAY)
            drawCircle(dimension.left+dimension.single/2f, dimension.top+dimension.single/2f, radius, paint)

            for (x in 0 until solution.xCount) {
                val runs = solution.colRuns(x)

                // TODO translation back and forth from game and dimension space
                val sx = dimension.left + (x + 1)*dimension.single
                val sy = dimension.top

                drawRect(area.with(sx, sy, sx+dimension.single, sy+dimension.single), thinPaint)

                if (runs.isEmpty()) {
                    continue
                }

                Helper.rangesInRect(canvas, runs, area, textPaint, false)
            }

            for (y in 0 until solution.yCount) {
                val runs = solution.rowRuns(y)

                // TODO doubled above twice
                val sx = dimension.left
                val sy = dimension.top + (y + 1)*dimension.single

                drawRect(area.with(sx, sy, sx+dimension.single, sy+dimension.single), thinPaint)

                if (runs.isEmpty()) {
                    continue
                }

                Helper.rangesInRect(canvas, runs, area, textPaint, true)
            }

            for (x in 0 until game.xCount) {
                for (y in 0 until game.yCount) {
                    val sx = dimension.left + (x + 1)*dimension.single
                    val sy = dimension.top + (y + 1)*dimension.single
                    drawRect(area.with(sx, sy, sx+dimension.single, sy+dimension.single), linePaint.with(Color.BLUE))

                    val value = game.getField(x, y)
                    if (value != StateValue.EMPTY) {
                        //drawCircle(sx+dimension.single/2f, sy+dimension.single/2f, radius, fillPaint.with(0xff00560b.toInt()))
                        // TODO margin management
                        val cell = area.with(sx+padding, sy+padding, sx+dimension.single-padding, sy+dimension.single-padding)
                        if (value == StateValue.ON) {
                            drawRect(cell, fillPaint.with(0xff00560b.toInt()))
                        } else if (value == StateValue.OFF) {
                            val crossPaint = linePaint.with(0xffb3002b.toInt())
                            drawLine(cell.left, cell.top, cell.right, cell.bottom, crossPaint)
                            drawLine(cell.left, cell.bottom, cell.right, cell.top, crossPaint)
                        }
                    } else if (solution.isOn(x, y)) {
                        //drawCircle(sx+dimension.single/2f, sy+dimension.single/2f, radius, thinPaint)
                    }
                }
            }
        }
    }

    override fun onTouch(view: View, motion: MotionEvent): Boolean {
        // TODO support double tap?

        if (motion.action == MotionEvent.ACTION_MOVE) {
            val (xSquare, ySquare) = dimension.getLocation(motion)

            if (xSquare != lastX || ySquare != lastY) {
                if (dimension.isGameField(xSquare, ySquare)) {
                    val xGame = xSquare - 1
                    val yGame = ySquare - 1

                    game.switch(xGame, yGame)

                    invalidate()
                } else {
                    Log.w("B", "Irregular field")
                }

                lastX = xSquare
                lastY = ySquare
            }
        } else if (motion.action == MotionEvent.ACTION_UP) {
            // Reset for next run
            lastX = -1
            lastY = -1

            checkValid()
        }

        return true
    }

    fun create() {
        game.clear()
        solution.create(0.5f) // TODO setting
        invalidate()
    }

    fun clear() {
        game.clear()
        invalidate()
    }

    private fun checkValid() {
        // TODO have a more "dynamic" connection between this field and the ui display

        val nowValid = game.isValid(solution)
        if (nowValid != hasValidSolution) {
            hasValidSolution = nowValid
            invalidate()
        }
    }

    fun redraw() {
        invalidate()
    }

    fun restore(prefs: SharedPreferences) {
        if (hasFixedSettings) {
            return
        }

        // TODO serialization is bad!
        try {
            prefs.getString("game", null)?.let { game = Helper.deserialize(it) as State }
            prefs.getString("solution", null)?.let { solution = Helper.deserialize(it) as Solution }
            if (!game.isAlive()) {
                // Despite serialVersionUID this is some sort of failure to restore

                game =  State()
                solution = Solution()
            } else {
                checkValid()
            }
        } catch (e: InvalidClassException) {
            Log.w("B", "Deserialized game not valid")
        }
    }

    fun save(prefs: SharedPreferences.Editor) {
        prefs.putString("game", Helper.serialize(game))
        val x = Helper.serialize(solution)
        prefs.putString("solution", x)
    }
}

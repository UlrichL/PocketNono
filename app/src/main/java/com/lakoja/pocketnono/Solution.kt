package com.lakoja.pocketnono

import android.util.Log
import java.security.SecureRandom

class Solution: State() {
    @Transient private var myRandom: SecureRandom? = null // The current normal Random (on Android) has a bug and always returns the same numbers

    fun setup(w: Int, h: Int, onFactor: Float) {
        super.setup(w, h)

        create(onFactor)
    }

    fun create(onFactor: Float) {
        findProperSolution(onFactor)
    }

    private fun findProperSolution(onFactor: Float) {
        if (myRandom == null) {
            myRandom = SecureRandom()
            // Needed after deserialization; TODO is there a better solution?
        }

        var counter = 0
        do {
            fields.forEachIndexed{idx, _ -> fields[idx] = if (myRandom!!.nextFloat() <= onFactor) StateValue.ON else StateValue.OFF}
            // TODO should be repaired instead of simply looping
        } while(!isProper(onFactor) && counter++ < 10)

        if (!isProper(onFactor)) {
            // TODO some sort of real error?
            Log.e("S", "Could not find a proper game solution")
        }
    }

    private fun isProper(onFactor: Float): Boolean {
        // Must have at least a semblance of the correct count
        val should = fields.size * onFactor
        val itis =  fields.count{it == StateValue.ON}
        if (itis < should*0.9f || itis > should*1.15f) {
            return false
        }

        // Both columns and rows may have at most 3 or 4 runs each

        for (x in 0 until xCount) {
            if (colRuns(x).size > 4) {
                return false
            }
        }
        for (y in 0 until yCount) {
            if (rowRuns(y).size > 3) {
                return false
            }
        }

        return true
    }
}
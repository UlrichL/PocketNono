package com.lakoja.pocketnono

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import java.io.*
import java.util.*

class Helper {
    companion object {
        private val textRect = Rect()
        private val paintRect = RectF()

        /**
         * Places a text centered in a given rectangle.
         */
        fun textInRect(canvas: Canvas, text: String, targetRect: RectF, paint: Paint) {
            paint.getTextBounds(text, 0, text.length, textRect)
            val tx = targetRect.width() / 2f - textRect.width() / 2f - textRect.left
            val ty = targetRect.height() / 2f + textRect.height() / 2f - textRect.bottom

            canvas.drawText(text, targetRect.left + tx, targetRect.top + ty, paint)
        }

        /**
         * Does a "ranges display" in a given rectangle: Up to four numbers are shown as up to two columns or rows.
         * Everything is centered in its respective (sub) region.
         */
        fun rangesInRect(canvas: Canvas, ranges: Array<Int>, targetRect: RectF, paint: Paint, horizontal: Boolean) {
            val w = targetRect.width()
            val h = targetRect.height()

            when (ranges.size) {
                0 -> return
                1 -> textInRect(canvas, ranges[0].toString(), targetRect, paint)
                2 -> {
                    paintRect.with(targetRect.left, targetRect.top, targetRect.left + if (horizontal) w/2 else w, targetRect.top + if (horizontal) h else h/2)
                    textInRect(canvas, ranges[0].toString(), paintRect, paint)

                    val x = targetRect.left + if (horizontal) w/2 else 0f
                    val y = targetRect.top + if (horizontal) 0f else h/2
                    paintRect.with(x, y, x + if (horizontal) w/2 else w, y + if (horizontal) h else h/2)
                    textInRect(canvas, ranges[1].toString(), paintRect, paint)
                }
                3 -> {
                    paintRect.with(targetRect.left, targetRect.top, targetRect.left + w/2, targetRect.top + h/2)
                    textInRect(canvas, ranges[0].toString(), paintRect, paint)

                    var x = targetRect.left + if (horizontal) w/2 else 0f
                    var y = targetRect.top + if (horizontal) 0f else h/2
                    paintRect.with(x, y, x + w/2, y + h/2)
                    textInRect(canvas, ranges[1].toString(), paintRect, paint)

                    x = targetRect.left + if (horizontal) 0f else w/2
                    y = targetRect.top + if (horizontal) h/2 else  0f
                    paintRect.with(x, y, x + if (horizontal) w else w/2, y + if (horizontal) h/2 else h)
                    textInRect(canvas, ranges[2].toString(), paintRect, paint)
                }
                else -> {
                    paintRect.with(targetRect.left, targetRect.top, targetRect.left + w/2, targetRect.top + h/2)
                    textInRect(canvas, ranges[0].toString(), paintRect, paint)

                    var x = targetRect.left + if (horizontal) w/2 else 0f
                    var y = targetRect.top + if (horizontal) 0f else h/2
                    paintRect.with(x, y, x + w/2, y + h/2)
                    textInRect(canvas, ranges[1].toString(), paintRect, paint)

                    x = targetRect.left + if (horizontal) 0f else w/2
                    y = targetRect.top + if (horizontal) h/2 else 0f
                    paintRect.with(x, y, x + w/2, y + h/2)
                    textInRect(canvas, ranges[2].toString(), paintRect, paint)

                    paintRect.with(targetRect.left + w/2, targetRect.top + h/2, targetRect.left + w, targetRect.top + h)
                    textInRect(canvas, ranges[3].toString(), paintRect, paint)
                }
            }
        }

        // TODO extremely nasty; also should be JSON:
        fun deserialize(s: String): Any? {
            val data: ByteArray = Base64.getDecoder().decode(s)
            val ois = ObjectInputStream(ByteArrayInputStream(data))
            val obj: Any = ois.readObject()
            ois.close()

            return obj
        }

        fun serialize(obj: Serializable): String? {
            val bos = ByteArrayOutputStream()
            val oos = ObjectOutputStream(bos)
            oos.writeObject(obj)
            oos.close()

            return Base64.getEncoder().encodeToString(bos.toByteArray())
        }
    }
}
package com.lakoja.pocketnono

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.lakoja.pocketnono.databinding.FragmentWelcomeBinding

class WelcomeFragment : Fragment() {
    private var _binding: FragmentWelcomeBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentWelcomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonNewGame.setOnClickListener {
            Board.instance?.create()
            findNavController().navigate(R.id.action_Welcome_to_Game)
        }

        // TODO disable if there is no active game state
        binding.buttonContinue.setOnClickListener {
            findNavController().navigate(R.id.action_Welcome_to_Game)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
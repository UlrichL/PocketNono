package com.lakoja.pocketnono

open class State: java.io.Serializable {
    companion object {
        const val serialVersionUID: Long = 1
    }

    var xCount = 0
        protected set
    var yCount = 0
        protected set
    protected var fields = emptyArray<StateValue>()

    fun matches(w: Int, h: Int): Boolean {
        return w == xCount && h == yCount
    }

    fun setup(w: Int, h: Int) {
        xCount = w
        yCount = h
        fields = Array(w*h) { _ -> StateValue.EMPTY }
    }

    fun setup(w: Int, h: Int, fs: Array<StateValue>) {
        xCount = w
        yCount = h
        fields = fs
    }

    fun isAlive(): Boolean {
        // TODO as written elsewhere: Serialization is nasty and needs to be replaced (?)

        return fields != null
    }

    fun clear() {
        fields.forEachIndexed { index, _ -> fields[index] = StateValue.EMPTY }
    }

    fun switch(x: Int, y:Int) {
        val pos = x+y*xCount
        if (pos < 0 || pos >= fields.size) {
            return
        }
        fields[pos] = fields[pos].switch()
    }

    fun isOn(x: Int, y:Int): Boolean {
        return getField(x, y) == StateValue.ON
    }

    fun getField(x: Int, y:Int): StateValue {
        val pos = x + y * xCount
        if (pos < 0 || pos >= fields.size) {
            return StateValue.EMPTY
        }

        return fields[pos]
    }

    fun isValid(game: State): Boolean {
        val otherCount = game.fields.count{it == StateValue.ON}
        val thisCount = fields.count{it == StateValue.ON}

        if (otherCount == 0 || thisCount == 0) {
            return false
        }

        if (otherCount != thisCount) {
            return false
        }

        // TODO more proper row and col check
        return game.fields contentEquals fields
    }

//    private fun invertIndexes(runs: Array<Int>, length: Int): Any {
//
//    }

    fun rowCount(row: Int): Int {
        if (row < 0 || row >= yCount) {
            return 0
        }

        return fields.slice(IntRange(row*xCount, (row+1)*xCount-1)).count{it == StateValue.ON}
    }

    fun colRuns(col: Int): Array<Int> {
        if (col < 0 || col >= xCount) {
            return emptyArray()
        }

        val indexes = getIndexes(col, -1)

        return getRuns(indexes).toTypedArray()
    }

    fun rowRuns(row: Int): Array<Int> {
        if (row < 0 || row >= yCount) {
            return emptyArray()
        }

        val indexes = getIndexes(-1, row)

        return getRuns(indexes).toTypedArray()
    }

    private fun getIndexes(col: Int, row: Int): List<Int> {
        if (col == -1) {
            return (row*xCount until (row+1)*xCount).toList()
        }

        return (col..col+(yCount-1)*xCount step xCount).toList()
    }

    private fun getRuns(indexes: Iterable<Int>): List<Int> {
        val runs = mutableListOf<Int>()
        var count = 0
        for (pos in indexes) {
            if (fields[pos] == StateValue.ON) {
                count += 1
            } else {
                if (count > 0) {
                    runs.add(count)
                    count = 0
                }
            }
        }
        if (count > 0) {
            runs.add(count)
        }

        return runs
    }
}
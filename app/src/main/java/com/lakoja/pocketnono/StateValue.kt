package com.lakoja.pocketnono

enum class StateValue(val value:Byte) {
    OFF(2),
    ON(1),
    EMPTY(0);

    companion object {
        private val map = values().associateBy { it.value }
        infix fun from(value: Byte) = map.getOrDefault(value, EMPTY)
    }

    fun switch(): StateValue {
        return when(value) {
            OFF.value -> EMPTY
            ON.value -> OFF
            else -> ON
        }
    }
}